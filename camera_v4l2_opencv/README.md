# camera_v4l2_opencv
V4L2 例程，并通过OpenCV 方式显示，代码含get camera capture timestamp

编译：
make 

命令：
./camera_v4l2_opencv -d /dev/video0 -s 1920x1080 -f UYVY -n 30 -c 

不同相机可能需要修改分辨率和YUV顺序
例程默认安装的OpenCV 4.3，其他版本编译如有报错需自行修改，安装方法可参考：
https://www.jianshu.com/p/de50451c3aa4
